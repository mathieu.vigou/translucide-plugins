<?
/* Plugin pour flux rss */
include_once("../config.php");// Variables
include_once("../api/function.php");// Fonctions
include_once("../api/db.php");// Connexion à la db

header('Content-Type:text/xml;');

echo"<?xml version='1.0' encoding='UTF-8'?>"
."\n<rss version='2.0'>"
."\n<channel>"
."<title>Le Grand Bain</title>"
."<description>Dernières actualités</description>"
."<link>".$GLOBALS['home']."</link>";

//@todo ajouter les produits et les url directe des fichiers

$sql = 	"SELECT SQL_CALC_FOUND_ROWS c.*
		FROM ".$tc." c 		
		WHERE (c.type='article' or c.type='event')
		AND c.lang='".$lang."' 
		AND c.state='active' 
		ORDER BY date_insert DESC";

$sel = $connect->query($sql);

while($res = $sel->fetch_assoc()) 
{
	$date_insert = explode(" ", $res['date_insert']);

	$content_event = json_decode($res['content'],true);

	echo"<item>"
	."\n<title>".str_replace(["&nbsp;","&"], ["","et"],$res['title'])."</title>"
	."\n<description>".str_replace(["&nbsp;","&"], ["","et"], word_cut($content_event['texte'], '250'))."…</description>"
	."\n<link>".make_url($res['url'], array("domaine" => true))."</link>"
	."\n<pubDate>".date('D, d M Y h:i:s +0000',strtotime($res['date_insert']))."</pubDate>" //Thu, 12 Jan 2023 08:46:40 +0000
	."\n</item>";

}

echo"</channel>\n</rss>";
?>